﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace BMIcalculatorLab5advLab6.Droid
{
    [Activity(Label="BMI Calculator",Theme ="@style/Splash", Icon ="@drawable/ic_launcher" ,MainLauncher =true)]
    public class SplashActivity:Activity
    { 
        public SplashActivity()
        {

        }
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            StartActivity(typeof(MainActivity));
            Finish();


        }
    }
}