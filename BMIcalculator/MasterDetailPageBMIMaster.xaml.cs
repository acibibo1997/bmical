﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BMIcalculatorLab5advLab6
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailPageBMIMaster : ContentPage
    {
        public ListView ListView;

        public MasterDetailPageBMIMaster()
        {
            InitializeComponent();

            BindingContext = new MasterDetailPageBMIMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MasterDetailPageBMIMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MasterDetailPageBMIMasterMenuItem> MenuItems { get; set; }

            public MasterDetailPageBMIMasterViewModel()
            {
                MenuItems = new ObservableCollection<MasterDetailPageBMIMasterMenuItem>(new[]
                {
                    new MasterDetailPageBMIMasterMenuItem { Id = 0, Title = "BMI Calculator", TargetType=typeof(MainPage)},
                    new MasterDetailPageBMIMasterMenuItem { Id = 1, Title = "Information", TargetType=typeof(Information)},
                    new MasterDetailPageBMIMasterMenuItem { Id = 2, Title = "Record", TargetType= typeof(TabbedRecord)} // it should be record, but since i forgot to change the file name.. gg 
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}