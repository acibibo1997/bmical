﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BMIcalculatorLab5advLab6
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new MainPage();
            //MainPage = new NavigationPage(new MainPage());
           MainPage = new MasterDetailPageBMI();

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
