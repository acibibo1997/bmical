﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BMIcalculatorLab5advLab6
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Information : ContentPage
    {
        public Information()
        {
            InitializeComponent();
        }

        async void OnYogaClicked(object sender, EventArgs args)
        {
            await 
                Browser.OpenAsync("https://www.youtube.com/watch?v=v7AYKMP6rOE",
                BrowserLaunchMode.SystemPreferred);

        }

        async void OnFoodClicked(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new FoodPage());

        }

        async void OnGymClicked(object sender, EventArgs args)
        {
            var placemark = new Placemark
            {
                CountryName = "Malaysia",
                AdminArea = null,
                Thoroughfare = "Jalan Ikram-UNITEN",
                Locality = "Kajang"
            };
            var options = new MapLaunchOptions { Name = "Dewan Seri Sarjana" };

            await Map.OpenAsync(placemark, options);


        }
    }
    

}