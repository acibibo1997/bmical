﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BMIcalculatorLab5advLab6
{

    public class MasterDetailPageBMIMasterMenuItem
    {
        public MasterDetailPageBMIMasterMenuItem()
        {
            TargetType = typeof(MasterDetailPageBMIMasterMenuItem);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}