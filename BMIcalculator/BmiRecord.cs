﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BMIcalculatorLab5advLab6
{
    public class BmiRecord
    {
        public string DateRecorded { get; set; }
        public double Weight { get; set; }
        public double BmiResult { get; set; }
        public string BmiStatus { get; set; }
    }
}
