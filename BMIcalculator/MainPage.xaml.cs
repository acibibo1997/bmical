﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.IO; //for the file transfer things

namespace BMIcalculatorLab5advLab6
{
    
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
       //string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),"BMIRecord.txt"); //kita set path to the file kat pc
        FireBaseHelper firebaseHelper = new FireBaseHelper();
        public MainPage()
        {
            InitializeComponent();
            
        }
  

        void OnCalculateBMI(object sender, EventArgs e)
        {
            var weight = 0.0; 
            var height = 0.0; 
            var bmiresult = 0.0;
            
            

            if((Double.TryParse(inputWeight.Text,out weight))&&(Double.TryParse(inputHeight.Text, out height)))
                {
                bmiresult = weight / (height * height);
                outputResult.Text = string.Format("{0:##.00}", bmiresult);
                }
                else
                {
                    outputResult.Text = "Please Enter A Valid Value";
                }

            if (bmiresult < 18.5)
            {
                outputBmiStatus.Text = "Underweight";
                outputBmiStatus.BackgroundColor = Color.Yellow;
            }

            else if ((bmiresult >= 18.5) && (bmiresult < 25))
            {
                outputBmiStatus.Text = "Normal";
                outputBmiStatus.BackgroundColor = Color.Green;
            }

            else if ((bmiresult >= 25) && (bmiresult < 30))
            {
                outputBmiStatus.Text = "Overweight";
                outputBmiStatus.BackgroundColor = Color.Orange;
            }

            else 
            {
                outputBmiStatus.Text = "Obese";
                outputBmiStatus.BackgroundColor = Color.Red;
            }

        }
        void OnReset(object sender, EventArgs e)
        {
            inputWeight.Text = null;
            inputHeight.Text = null;
            outputResult.Text = "0.00";
            outputBmiStatus.Text = "Not Available";
            outputBmiStatus.BackgroundColor = Color.Transparent;
            outputBmiStatus.TextColor = default;

        }

        void OnDatePickerSelected(Object sender, DateChangedEventArgs e)
        {
            var selectedDate = e.NewDate.ToString();
        }

        async void OnSaveRecord(object sender, EventArgs e) //function to store data to the BMIresult.txt
        {
            /* var writerRecord = selectDate.Date.ToString("dd/MM/yyyy") +
                 "\nWeight: " + inputWeight.Text + " kg " +
                 "\nBMI Value: " + inputHeight.Text + " m" +
                 "\nBMI Status: " + outputBmiStatus.Text +
                 "\n";
             File.AppendAllText(fileName, writerRecord + Environment.NewLine);*/
            var selectdate = selectDate.Date.ToString("dd/MM/yyyy");
            var weight = Double.Parse(inputWeight.Text);
            var bmiresult = Double.Parse(outputResult.Text);
            string bmistatus = outputBmiStatus.Text;
            await firebaseHelper.AddRecord(selectdate, weight, bmiresult, bmistatus);
            await DisplayAlert("Record Saved", "BMI Record HAs Been Saved", "OK");


        }
    }
}
